import QtQuick.tooling 1.2

// This file describes the plugin-supplied types contained in the library.
// It is used for QML tooling purposes only.
//
// This file was auto-generated by:
// 'qmlplugindump -nonrelocatable QtStudio3D.OpenGL 2.5'

Module {
    dependencies: []
    Component {
        name: "Q3DSDataInput"
        prototype: "QObject"
        exports: ["QtStudio3D.OpenGL/DataInput 2.4"]
        exportMetaObjectRevisions: [0]
        Property { name: "name"; type: "string" }
        Property { name: "value"; type: "QVariant" }
        Property { name: "max"; type: "float"; isReadonly: true }
        Property { name: "min"; type: "float"; isReadonly: true }
        Property { name: "metadataKeys"; type: "QStringList"; isReadonly: true }
        Method {
            name: "setName"
            Parameter { name: "name"; type: "string" }
        }
        Method {
            name: "setValue"
            Parameter { name: "value"; type: "QVariant" }
            Parameter { name: "force"; type: "bool" }
        }
        Method {
            name: "setValue"
            Parameter { name: "value"; type: "QVariant" }
        }
        Method {
            name: "metadata"
            type: "string"
            Parameter { name: "key"; type: "string" }
        }
    }
    Component {
        name: "Q3DSDataOutput"
        prototype: "QObject"
        exports: ["QtStudio3D.OpenGL/DataOutput 2.4"]
        exportMetaObjectRevisions: [0]
        Property { name: "name"; type: "string" }
        Property { name: "value"; type: "QVariant"; isReadonly: true }
        Signal {
            name: "nameChanged"
            Parameter { name: "newName"; type: "string" }
        }
        Signal {
            name: "valueChanged"
            Parameter { name: "newValue"; type: "QVariant" }
        }
        Method {
            name: "setName"
            Parameter { name: "name"; type: "string" }
        }
    }
    Component {
        name: "Q3DSElement"
        prototype: "QObject"
        exports: ["QtStudio3D.OpenGL/Element 2.4"]
        exportMetaObjectRevisions: [0]
        Property { name: "elementPath"; type: "string" }
        Signal {
            name: "elementPathChanged"
            Parameter { name: "elementPath"; type: "string" }
        }
        Method {
            name: "setElementPath"
            Parameter { name: "elementPath"; type: "string" }
        }
        Method {
            name: "setAttribute"
            Parameter { name: "attributeName"; type: "string" }
            Parameter { name: "value"; type: "QVariant" }
        }
        Method {
            name: "fireEvent"
            Parameter { name: "eventName"; type: "string" }
        }
    }
    Component {
        name: "Q3DSPresentation"
        prototype: "QObject"
        Property { name: "source"; type: "QUrl" }
        Property { name: "variantList"; type: "QStringList" }
        Property { name: "delayedLoading"; type: "bool" }
        Property { name: "createdElements"; type: "QStringList"; isReadonly: true }
        Property { name: "createdMaterials"; type: "QStringList"; isReadonly: true }
        Property { name: "createdMeshes"; type: "QStringList"; isReadonly: true }
        Property { name: "shaderCacheFile"; revision: 1; type: "QUrl" }
        Signal {
            name: "variantListChanged"
            Parameter { name: "variantList"; type: "QStringList" }
        }
        Signal {
            name: "sourceChanged"
            Parameter { name: "source"; type: "QUrl" }
        }
        Signal {
            name: "slideEntered"
            Parameter { name: "elementPath"; type: "string" }
            Parameter { name: "index"; type: "uint" }
            Parameter { name: "name"; type: "string" }
        }
        Signal {
            name: "slideExited"
            Parameter { name: "elementPath"; type: "string" }
            Parameter { name: "index"; type: "uint" }
            Parameter { name: "name"; type: "string" }
        }
        Signal { name: "dataInputsReady" }
        Signal { name: "dataOutputsReady" }
        Signal {
            name: "customSignalEmitted"
            Parameter { name: "elementPath"; type: "string" }
            Parameter { name: "name"; type: "string" }
        }
        Signal {
            name: "delayedLoadingChanged"
            Parameter { name: "enable"; type: "bool" }
        }
        Signal {
            name: "elementsCreated"
            Parameter { name: "elementPaths"; type: "QStringList" }
            Parameter { name: "error"; type: "string" }
        }
        Signal {
            name: "materialsCreated"
            Parameter { name: "materialNames"; type: "QStringList" }
            Parameter { name: "error"; type: "string" }
        }
        Signal {
            name: "meshesCreated"
            Parameter { name: "meshNames"; type: "QStringList" }
            Parameter { name: "error"; type: "string" }
        }
        Signal {
            name: "shaderCacheFileChanged"
            Parameter { name: "fileName"; type: "QUrl" }
        }
        Signal {
            name: "shaderCacheExported"
            Parameter { name: "success"; type: "bool" }
        }
        Signal {
            name: "shaderCacheLoadErrors"
            Parameter { name: "errors"; type: "string" }
        }
        Method {
            name: "setSource"
            Parameter { name: "source"; type: "QUrl" }
        }
        Method {
            name: "setVariantList"
            Parameter { name: "variantList"; type: "QStringList" }
        }
        Method {
            name: "goToSlide"
            Parameter { name: "elementPath"; type: "string" }
            Parameter { name: "index"; type: "uint" }
        }
        Method {
            name: "goToSlide"
            Parameter { name: "elementPath"; type: "string" }
            Parameter { name: "name"; type: "string" }
        }
        Method {
            name: "goToSlide"
            Parameter { name: "elementPath"; type: "string" }
            Parameter { name: "next"; type: "bool" }
            Parameter { name: "wrap"; type: "bool" }
        }
        Method {
            name: "goToTime"
            Parameter { name: "elementPath"; type: "string" }
            Parameter { name: "time"; type: "float" }
        }
        Method {
            name: "setAttribute"
            Parameter { name: "elementPath"; type: "string" }
            Parameter { name: "attributeName"; type: "string" }
            Parameter { name: "value"; type: "QVariant" }
        }
        Method {
            name: "setPresentationActive"
            Parameter { name: "id"; type: "string" }
            Parameter { name: "active"; type: "bool" }
        }
        Method {
            name: "fireEvent"
            Parameter { name: "elementPath"; type: "string" }
            Parameter { name: "eventName"; type: "string" }
        }
        Method {
            name: "setGlobalAnimationTime"
            Parameter { name: "milliseconds"; type: "qlonglong" }
        }
        Method {
            name: "setDataInputValue"
            Parameter { name: "name"; type: "string" }
            Parameter { name: "value"; type: "QVariant" }
            Parameter { name: "force"; type: "bool" }
        }
        Method {
            name: "setDataInputValue"
            Parameter { name: "name"; type: "string" }
            Parameter { name: "value"; type: "QVariant" }
        }
        Method {
            name: "setShaderCacheFile"
            Parameter { name: "fileName"; type: "QUrl" }
        }
        Method { name: "getDataInputs"; type: "QVariantList" }
        Method {
            name: "getDataInputs"
            type: "QVariantList"
            Parameter { name: "metadataKey"; type: "string" }
        }
        Method { name: "getDataOutputs"; type: "QVariantList" }
        Method {
            name: "preloadSlide"
            Parameter { name: "elementPath"; type: "string" }
        }
        Method {
            name: "unloadSlide"
            Parameter { name: "elementPath"; type: "string" }
        }
        Method {
            name: "exportShaderCache"
            revision: 1
            Parameter { name: "shaderCacheFile"; type: "QUrl" }
            Parameter { name: "binaryShaders"; type: "bool" }
        }
        Method {
            name: "exportShaderCache"
            revision: 1
            Parameter { name: "binaryShaders"; type: "bool" }
        }
        Method {
            name: "exportShaderCache"
            revision: 2
            Parameter { name: "shaderCacheFile"; type: "QUrl" }
            Parameter { name: "binaryShaders"; type: "bool" }
            Parameter { name: "compressionLevel"; type: "int" }
        }
        Method {
            name: "exportShaderCache"
            revision: 2
            Parameter { name: "binaryShaders"; type: "bool" }
            Parameter { name: "compressionLevel"; type: "int" }
        }
    }
    Component {
        name: "Q3DSPresentationItem"
        defaultProperty: "qmlChildren"
        prototype: "Q3DSPresentation"
        exports: [
            "QtStudio3D.OpenGL/Presentation 2.4",
            "QtStudio3D.OpenGL/Presentation 2.5",
            "QtStudio3D.OpenGL/Presentation 2.7"
        ]
        exportMetaObjectRevisions: [0, 1, 2]
        Property { name: "qmlChildren"; type: "QObject"; isList: true; isReadonly: true }
        Method {
            name: "appendQmlChildren"
            Parameter { name: "list"; type: "QObject"; isList: true; isPointer: true }
            Parameter { name: "obj"; type: "QObject"; isPointer: true }
        }
    }
    Component {
        name: "Q3DSQmlStream"
        defaultProperty: "item"
        prototype: "QObject"
        exports: ["QtStudio3D.OpenGL/QmlStream 2.4"]
        exportMetaObjectRevisions: [0]
        Property { name: "presentationId"; type: "string" }
        Property { name: "item"; type: "QQuickItem"; isPointer: true }
        Signal {
            name: "presentationIdChanged"
            Parameter { name: "presentationId"; type: "string" }
        }
        Signal {
            name: "itemChanged"
            Parameter { name: "item"; type: "QQuickItem"; isPointer: true }
        }
        Method {
            name: "setPresentationId"
            Parameter { name: "presentationId"; type: "string" }
        }
        Method {
            name: "setItem"
            Parameter { name: "item"; type: "QQuickItem"; isPointer: true }
        }
    }
    Component {
        name: "Q3DSSceneElement"
        prototype: "Q3DSElement"
        exports: ["QtStudio3D.OpenGL/SceneElement 2.4"]
        exportMetaObjectRevisions: [0]
        Property { name: "currentSlideIndex"; type: "int" }
        Property { name: "previousSlideIndex"; type: "int"; isReadonly: true }
        Property { name: "currentSlideName"; type: "string" }
        Property { name: "previousSlideName"; type: "string"; isReadonly: true }
        Signal {
            name: "currentSlideIndexChanged"
            Parameter { name: "currentSlideIndex"; type: "int" }
        }
        Signal {
            name: "previousSlideIndexChanged"
            Parameter { name: "previousSlideIndex"; type: "int" }
        }
        Signal {
            name: "currentSlideNameChanged"
            Parameter { name: "currentSlideName"; type: "string" }
        }
        Signal {
            name: "previousSlideNameChanged"
            Parameter { name: "previousSlideName"; type: "string" }
        }
        Method {
            name: "setCurrentSlideIndex"
            Parameter { name: "currentSlideIndex"; type: "int" }
        }
        Method {
            name: "setCurrentSlideName"
            Parameter { name: "currentSlideName"; type: "string" }
        }
        Method {
            name: "goToSlide"
            Parameter { name: "next"; type: "bool" }
            Parameter { name: "wrap"; type: "bool" }
        }
        Method {
            name: "goToTime"
            Parameter { name: "time"; type: "float" }
        }
    }
    Component {
        name: "Q3DSStudio3D"
        defaultProperty: "data"
        prototype: "QQuickFramebufferObject"
        exports: [
            "QtStudio3D.OpenGL/Studio3D 2.4",
            "QtStudio3D.OpenGL/Studio3D 2.5"
        ]
        exportMetaObjectRevisions: [0, 1]
        Enum {
            name: "EventIgnoreFlags"
            values: {
                "EnableAllEvents": 0,
                "IgnoreMouseEvents": 1,
                "IgnoreWheelEvents": 2,
                "IgnoreKeyboardEvents": 4,
                "IgnoreAllInputEvents": 7
            }
        }
        Property { name: "running"; type: "bool"; isReadonly: true }
        Property { name: "presentation"; type: "Q3DSPresentationItem"; isReadonly: true; isPointer: true }
        Property { name: "viewerSettings"; type: "Q3DSViewerSettings"; isReadonly: true; isPointer: true }
        Property { name: "error"; type: "string"; isReadonly: true }
        Property { name: "ignoredEvents"; type: "EventIgnoreFlags" }
        Property { name: "asyncInit"; revision: 1; type: "bool" }
        Signal { name: "frameUpdate" }
        Signal { name: "frameDraw" }
        Signal {
            name: "runningChanged"
            Parameter { name: "initialized"; type: "bool" }
        }
        Signal {
            name: "errorChanged"
            Parameter { name: "error"; type: "string" }
        }
        Signal { name: "presentationReady" }
        Signal { name: "presentationLoaded" }
        Signal {
            name: "asyncInitChanged"
            Parameter { name: "enabled"; type: "bool" }
        }
        Method { name: "reset" }
    }
    Component {
        name: "Q3DSSubPresentationSettings"
        prototype: "QObject"
        exports: ["QtStudio3D.OpenGL/SubPresentationSettings 2.4"]
        exportMetaObjectRevisions: [0]
        Property { name: "qmlStreams"; type: "Q3DSQmlStream"; isList: true; isReadonly: true }
    }
    Component {
        name: "Q3DSViewerSettings"
        prototype: "QObject"
        exports: [
            "QtStudio3D.OpenGL/ViewerSettings 2.4",
            "QtStudio3D.OpenGL/ViewerSettings 2.5",
            "QtStudio3D.OpenGL/ViewerSettings 2.7"
        ]
        exportMetaObjectRevisions: [0, 1, 2]
        Enum {
            name: "ShadeMode"
            values: {
                "ShadeModeShaded": 0,
                "ShadeModeShadedWireframe": 1
            }
        }
        Enum {
            name: "ScaleMode"
            values: {
                "ScaleModeFit": 0,
                "ScaleModeFill": 1,
                "ScaleModeCenter": 2
            }
        }
        Enum {
            name: "StereoMode"
            values: {
                "StereoModeMono": 0,
                "StereoModeTopBottom": 1,
                "StereoModeLeftRight": 2,
                "StereoModeAnaglyphRedCyan": 3,
                "StereoModeAnaglyphGreenMagenta": 4
            }
        }
        Property { name: "matteEnabled"; type: "bool" }
        Property { name: "matteColor"; type: "QColor" }
        Property { name: "showRenderStats"; type: "bool" }
        Property { name: "scaleMode"; type: "ScaleMode" }
        Property { name: "stereoMode"; revision: 1; type: "StereoMode" }
        Property { name: "stereoEyeSeparation"; revision: 1; type: "double" }
        Property { name: "stereoProgressiveEnabled"; revision: 2; type: "bool" }
        Property { name: "skipFramesInterval"; revision: 2; type: "int" }
        Signal {
            name: "matteEnabledChanged"
            Parameter { name: "enabled"; type: "bool" }
        }
        Signal {
            name: "matteColorChanged"
            Parameter { name: "color"; type: "QColor" }
        }
        Signal {
            name: "showRenderStatsChanged"
            Parameter { name: "show"; type: "bool" }
        }
        Signal {
            name: "shadeModeChanged"
            Parameter { name: "mode"; type: "ShadeMode" }
        }
        Signal {
            name: "scaleModeChanged"
            Parameter { name: "mode"; type: "ScaleMode" }
        }
        Signal {
            name: "stereoModeChanged"
            Parameter { name: "mode"; type: "StereoMode" }
        }
        Signal {
            name: "stereoEyeSeparationChanged"
            Parameter { name: "separation"; type: "double" }
        }
        Signal {
            name: "stereoProgressiveEnabledChanged"
            revision: 2
            Parameter { name: "enabled"; type: "bool" }
        }
        Signal {
            name: "skipFramesIntervalChanged"
            revision: 2
            Parameter { name: "interval"; type: "int" }
        }
        Method {
            name: "setMatteEnabled"
            Parameter { name: "enabled"; type: "bool" }
        }
        Method {
            name: "setMatteColor"
            Parameter { name: "color"; type: "QColor" }
        }
        Method {
            name: "setShowRenderStats"
            Parameter { name: "show"; type: "bool" }
        }
        Method {
            name: "setScaleMode"
            Parameter { name: "mode"; type: "ScaleMode" }
        }
        Method {
            name: "setStereoMode"
            Parameter { name: "mode"; type: "StereoMode" }
        }
        Method {
            name: "setStereoEyeSeparation"
            Parameter { name: "separation"; type: "double" }
        }
        Method {
            name: "setStereoProgressiveEnabled"
            revision: 2
            Parameter { name: "enabled"; type: "bool" }
        }
        Method {
            name: "setSkipFramesInterval"
            revision: 2
            Parameter { name: "interval"; type: "int" }
        }
        Method {
            name: "save"
            Parameter { name: "group"; type: "string" }
            Parameter { name: "organization"; type: "string" }
            Parameter { name: "application"; type: "string" }
        }
        Method {
            name: "save"
            Parameter { name: "group"; type: "string" }
            Parameter { name: "organization"; type: "string" }
        }
        Method {
            name: "save"
            Parameter { name: "group"; type: "string" }
        }
        Method {
            name: "load"
            Parameter { name: "group"; type: "string" }
            Parameter { name: "organization"; type: "string" }
            Parameter { name: "application"; type: "string" }
        }
        Method {
            name: "load"
            Parameter { name: "group"; type: "string" }
            Parameter { name: "organization"; type: "string" }
        }
        Method {
            name: "load"
            Parameter { name: "group"; type: "string" }
        }
    }
    Component {
        name: "QQuickFramebufferObject"
        defaultProperty: "data"
        prototype: "QQuickItem"
        Property { name: "textureFollowsItemSize"; type: "bool" }
        Property { name: "mirrorVertically"; type: "bool" }
        Signal {
            name: "textureFollowsItemSizeChanged"
            Parameter { type: "bool" }
        }
        Signal {
            name: "mirrorVerticallyChanged"
            Parameter { type: "bool" }
        }
    }
    Component {
        name: "QQuickItem"
        defaultProperty: "data"
        prototype: "QObject"
        Enum {
            name: "Flags"
            values: {
                "ItemClipsChildrenToShape": 1,
                "ItemAcceptsInputMethod": 2,
                "ItemIsFocusScope": 4,
                "ItemHasContents": 8,
                "ItemAcceptsDrops": 16
            }
        }
        Enum {
            name: "TransformOrigin"
            values: {
                "TopLeft": 0,
                "Top": 1,
                "TopRight": 2,
                "Left": 3,
                "Center": 4,
                "Right": 5,
                "BottomLeft": 6,
                "Bottom": 7,
                "BottomRight": 8
            }
        }
        Property { name: "parent"; type: "QQuickItem"; isPointer: true }
        Property { name: "data"; type: "QObject"; isList: true; isReadonly: true }
        Property { name: "resources"; type: "QObject"; isList: true; isReadonly: true }
        Property { name: "children"; type: "QQuickItem"; isList: true; isReadonly: true }
        Property { name: "x"; type: "double" }
        Property { name: "y"; type: "double" }
        Property { name: "z"; type: "double" }
        Property { name: "width"; type: "double" }
        Property { name: "height"; type: "double" }
        Property { name: "opacity"; type: "double" }
        Property { name: "enabled"; type: "bool" }
        Property { name: "visible"; type: "bool" }
        Property { name: "visibleChildren"; type: "QQuickItem"; isList: true; isReadonly: true }
        Property { name: "states"; type: "QQuickState"; isList: true; isReadonly: true }
        Property { name: "transitions"; type: "QQuickTransition"; isList: true; isReadonly: true }
        Property { name: "state"; type: "string" }
        Property { name: "childrenRect"; type: "QRectF"; isReadonly: true }
        Property { name: "anchors"; type: "QQuickAnchors"; isReadonly: true; isPointer: true }
        Property { name: "left"; type: "QQuickAnchorLine"; isReadonly: true }
        Property { name: "right"; type: "QQuickAnchorLine"; isReadonly: true }
        Property { name: "horizontalCenter"; type: "QQuickAnchorLine"; isReadonly: true }
        Property { name: "top"; type: "QQuickAnchorLine"; isReadonly: true }
        Property { name: "bottom"; type: "QQuickAnchorLine"; isReadonly: true }
        Property { name: "verticalCenter"; type: "QQuickAnchorLine"; isReadonly: true }
        Property { name: "baseline"; type: "QQuickAnchorLine"; isReadonly: true }
        Property { name: "baselineOffset"; type: "double" }
        Property { name: "clip"; type: "bool" }
        Property { name: "focus"; type: "bool" }
        Property { name: "activeFocus"; type: "bool"; isReadonly: true }
        Property { name: "activeFocusOnTab"; revision: 1; type: "bool" }
        Property { name: "rotation"; type: "double" }
        Property { name: "scale"; type: "double" }
        Property { name: "transformOrigin"; type: "TransformOrigin" }
        Property { name: "transformOriginPoint"; type: "QPointF"; isReadonly: true }
        Property { name: "transform"; type: "QQuickTransform"; isList: true; isReadonly: true }
        Property { name: "smooth"; type: "bool" }
        Property { name: "antialiasing"; type: "bool" }
        Property { name: "implicitWidth"; type: "double" }
        Property { name: "implicitHeight"; type: "double" }
        Property { name: "containmentMask"; revision: 11; type: "QObject"; isPointer: true }
        Property { name: "layer"; type: "QQuickItemLayer"; isReadonly: true; isPointer: true }
        Signal {
            name: "childrenRectChanged"
            Parameter { type: "QRectF" }
        }
        Signal {
            name: "baselineOffsetChanged"
            Parameter { type: "double" }
        }
        Signal {
            name: "stateChanged"
            Parameter { type: "string" }
        }
        Signal {
            name: "focusChanged"
            Parameter { type: "bool" }
        }
        Signal {
            name: "activeFocusChanged"
            Parameter { type: "bool" }
        }
        Signal {
            name: "activeFocusOnTabChanged"
            revision: 1
            Parameter { type: "bool" }
        }
        Signal {
            name: "parentChanged"
            Parameter { type: "QQuickItem"; isPointer: true }
        }
        Signal {
            name: "transformOriginChanged"
            Parameter { type: "TransformOrigin" }
        }
        Signal {
            name: "smoothChanged"
            Parameter { type: "bool" }
        }
        Signal {
            name: "antialiasingChanged"
            Parameter { type: "bool" }
        }
        Signal {
            name: "clipChanged"
            Parameter { type: "bool" }
        }
        Signal {
            name: "windowChanged"
            revision: 1
            Parameter { name: "window"; type: "QQuickWindow"; isPointer: true }
        }
        Signal { name: "containmentMaskChanged"; revision: 11 }
        Method { name: "update" }
        Method {
            name: "grabToImage"
            revision: 4
            type: "bool"
            Parameter { name: "callback"; type: "QJSValue" }
            Parameter { name: "targetSize"; type: "QSize" }
        }
        Method {
            name: "grabToImage"
            revision: 4
            type: "bool"
            Parameter { name: "callback"; type: "QJSValue" }
        }
        Method {
            name: "contains"
            type: "bool"
            Parameter { name: "point"; type: "QPointF" }
        }
        Method {
            name: "mapFromItem"
            Parameter { type: "QQmlV4Function"; isPointer: true }
        }
        Method {
            name: "mapToItem"
            Parameter { type: "QQmlV4Function"; isPointer: true }
        }
        Method {
            name: "mapFromGlobal"
            revision: 7
            Parameter { type: "QQmlV4Function"; isPointer: true }
        }
        Method {
            name: "mapToGlobal"
            revision: 7
            Parameter { type: "QQmlV4Function"; isPointer: true }
        }
        Method { name: "forceActiveFocus" }
        Method {
            name: "forceActiveFocus"
            Parameter { name: "reason"; type: "Qt::FocusReason" }
        }
        Method {
            name: "nextItemInFocusChain"
            revision: 1
            type: "QQuickItem*"
            Parameter { name: "forward"; type: "bool" }
        }
        Method { name: "nextItemInFocusChain"; revision: 1; type: "QQuickItem*" }
        Method {
            name: "childAt"
            type: "QQuickItem*"
            Parameter { name: "x"; type: "double" }
            Parameter { name: "y"; type: "double" }
        }
    }
}
